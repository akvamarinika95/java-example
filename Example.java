import java.util.Scanner;
public class Example{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Введите число 1: ");
        int num_a = input.nextInt();
        System.out.print("Введите число 2: ");
        int num_b = input.nextInt();
        System.out.println("Введите оператор \n " +
                "+ сложение \n" +
                "- вычитание \n" +
                "* умножение \n" +
                "/ деление \n" +
                "p возвести в степень\n" +
                "s извлечь корень");
        char operator = input.next().charAt(0);
        input.close();
        if (operator == '+'){
            addition(num_a, num_b);
        } else if (operator == '-'){
            subtraction(num_a, num_b);
        } else if (operator == '*'){
            multiplication(num_a,num_b);
        } else if (operator == '/'){
            division(num_a,num_b);
        } else if (operator == 'p'){
            int result = num_a;
            pow(num_a,num_b, result);
        } else if (operator == 's'){
            final int PW = num_b;
            double root = num_a / num_b;
            double tmp = num_a;
           sqrt(num_a, root, tmp, PW);

            // вычисление целого корня др. вариант:
            int result = 2;
            int res_pow = result;
            if(PW < 2)
                System.out.println("Корня такой степени не существует!");
            else
                calculatingRoot(num_a, num_b, result, res_pow, PW);
        } else {
            System.out.print("Некорректный ввод!");
        }
    }

    public static void addition(int num1, int num2){
        System.out.println(num1 + num2);
    }

    public static void subtraction(int num1, int num2){
        System.out.print(num1 - num2);
    }
    public static void multiplication(int num1, int num2){
        System.out.print(num1 * num2);

    }
    public static void division(int num1, int num2){
        System.out.print(num1 / num2);
    }

    public static void pow(int num, int power, int res){
        if (power == 1 || num == 0)
            System.out.println(res);
        else if(power == 0){
            System.out.println(1);
        }
        else {
            res = res * num;   // 5^3  res = res * num1 =>  res=5*5 => res = 25*5 => 125
           // System.out.println(num); // res *= num1 => res = 5*5 => res = 25*25 => 625  => res = res * res ???
            pow(num, --power, res);
        }
    }
   public static void calculatingRoot(int num, int power, int result, int res_pow, int pw) {
       if (res_pow == num)
           System.out.println(result);
       else if (power >= 2) {
           res_pow = res_pow * result;
           calculatingRoot(num, --power, result, res_pow, pw);
       }
       else if(result == num/power){
            System.out.println("Не удалось извлечь корень, вероятно он дробный");
       } else {
           power = pw;
           result += 1;
           res_pow = result;
           calculatingRoot(num, power, result, res_pow, pw);
      }
   }


    public static double abs(double num){
        return (num < 0)? -num : num;
    }

    public static double approxRoot(double num, double root, double tmp, int PW, int i){
        if (i < PW){
            //tmp = tmp / root;
            return approxRoot(num, root, tmp, PW, ++i)/root;
        }
        else return tmp;
    }


    public static void sqrt(double num, double root, double tmp, int PW){
        final double EPS = 1e-5;
        //System.out.println(root);
        if (abs(root - tmp)  >= EPS) {
            tmp = num;
            int i=1;
            //System.out.println(test(num,root,tmp, PW, i));
            tmp = approxRoot(num,root,tmp, PW, i);
            root =  (root + tmp) * 0.5;
            sqrt(num, root, tmp, PW);
        }
        else{
            System.out.println("*******************");
            System.out.println(root);
            System.out.println("*******************");
        }
    }


}



